# Frontend Exercise

## Requires

Node

## Instructions

Downloade the repository.

In the project directory, you can run:
### `npm intsall`
then:
### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## Other Notes

- I did not have the full testing I would like on the form validation and submission.
- There were a couple of custom components (e.g. button) that I would have created to make things cleaner.

