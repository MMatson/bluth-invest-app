export type Values = {
    investmentAmount: number;
    investmentType: string;
    totalNetWorth: number;
    estimatedAnnualIncome: number;
    estimatedCreditScore: number;
}
