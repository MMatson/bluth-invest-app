import {Values} from "./Values";

export type FormProps = {
    onSubmit: (values: Values) => void;
}