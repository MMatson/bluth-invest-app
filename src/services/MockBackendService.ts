import {Values} from "../model/Values";

export function getInvestorApprovalFromAPI(values: Values): Promise<String> {
    return new Promise((resolve, reject) => {
        if (values.investmentAmount > 9000000) {
            setTimeout(() => reject("Bad Request"), 2000);
        } else if (values.estimatedCreditScore < 600) {
            setTimeout(() => resolve("Disqualified"), 2000);
        } else if (values.investmentAmount > (0.2 * values.estimatedAnnualIncome)) {
            setTimeout(() => resolve("Disqualified"), 2000);
        } else if (values.investmentAmount > (0.03 * values.totalNetWorth)) {
            setTimeout(() => resolve("Disqualified"), 2000);
        } else {
            setTimeout(() => resolve("Approved"), 2000);
        }
    });
}
