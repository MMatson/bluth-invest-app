import React, {ReactNode} from 'react';
import '../styles/LogoCard.css';

type Props = {
    title: string;
    message?: string;
    children?: ReactNode;
}

function LogoCard({ ...props }: Props) {
    return (
        <div className="LogoCard" >
            <div className="logo">
                <img className="logoImage" src={"logo.jpg"}  alt={"company logo"}/>
            </div>
            <div className="title">
                <h1 >{props.title}</h1>
            </div>
            <div className="message">
                <p >{props.message}</p>
            </div>
            <div className="children">
                {props.children}
            </div>
        </div>
    );
}

export default LogoCard;
