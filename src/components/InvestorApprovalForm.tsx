import React from 'react';
import { Formik, Form, Field } from 'formik';
import { Button } from "@material-ui/core";
import FormField from "./FormField";
import { Values } from "../model/Values";
import { FormProps } from "../model/FormProps";
import * as yup from 'yup';
import "../styles/FormField.css"

const schema = yup.object({
    investmentAmount: yup.number()
        .typeError("Please enter a number")
        .required("Investment amount is required")
        .min(0, "Please enter a positive number"),
    investmentType: yup.string()
        .required("Investment type is required")
        .min(3, "Please use at least 3 characters to describe your investment type"),
    totalNetWorth: yup.number()
        .typeError("Please enter a number")
        .required("Total net worth is required")
        .min(0, "Please enter a positive number"),
    estimatedAnnualIncome: yup.number()
        .typeError("Please enter a number")
        .required("Est. annual income is required")
        .min(0, "Please enter a positive number"),
    estimatedCreditScore: yup.number()
        .typeError("Please enter a number")
        .required("Est. credit score is required")
        .min(300, "Credit Score must be between 300-850")
        .max(850, "Credit Score must be between 300-850")
})

function InvestorApprovalForm({ onSubmit }: FormProps) {
    let initialValues: Values = {
        investmentAmount: 0,
        investmentType: "Real Estate",
        totalNetWorth: 0,
        estimatedAnnualIncome: 0,
        estimatedCreditScore: 0
    }


    return (
        <Formik
            validationSchema={schema}
            initialValues={initialValues}
            onSubmit={ (values: Values, {setSubmitting}) => {
                setSubmitting(true);
                onSubmit(values);
                setSubmitting(false);
            }}
        >
            {({ values , isSubmitting}) => (
                <Form>
                    <div className="FormField">
                        <Field name="investmentAmount"
                               label="Investment Amount"
                               component={FormField}
                        />
                    </div>
                    <div className="FormField">
                        <Field name="investmentType"
                               label="Investment Type"
                               component={FormField}
                               data-testid="ia"
                        />
                    </div>
                    <div className="FormField">
                        <Field name="totalNetWorth"
                               label="Total Net Worth"
                               component={FormField}
                        />
                    </div>
                    <div className="FormField">
                        <Field name="estimatedAnnualIncome"
                               label="Estimated Annual Income"
                               component={FormField}
                        />
                    </div>
                    <div className="FormField">
                        <Field name="estimatedCreditScore"
                               label="Estimated Credit Score"
                               component={FormField}
                        />
                    </div>
                    <div className="FormField">
                        <Button className="button"
                                variant="outlined"
                                disableElevation
                                disabled={isSubmitting}
                                type="submit"
                        >
                            Submit
                        </Button>
                    </div>
                </Form>
            )}
        </Formik>
    );
}

export default InvestorApprovalForm;
