import React from 'react';
import { FieldProps, getIn } from "formik";
import { TextField, TextFieldProps } from "@material-ui/core";
import "../styles/FormField.css"

type Props = FieldProps & TextFieldProps & {
    placeholder: string;
}

function FormField({ placeholder, field, form, ...props }: Props) {
    const errorText = getIn(form.touched, field.name) && getIn(form.errors, field.name);

    return (
        <TextField className="textField"
                   variant="outlined"
            placeholder={placeholder}
            helperText={errorText}
            {...field}
            {...props}
        />
    );
}

export default FormField;
