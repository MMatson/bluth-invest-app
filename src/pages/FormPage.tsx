import React from 'react';
import InvestorApprovalForm from "../components/InvestorApprovalForm";
import { FormProps } from "../model/FormProps";
import LogoCard from "../components/LogoCard";


function FormPage({ onSubmit }: FormProps) {
    return (
        <div className="FormPage">
            <LogoCard title={"Investor Profile"}
                      message={"Please fill out the below form in order to begin the approval process."}>
                <InvestorApprovalForm onSubmit={values => {
                    onSubmit(values);
                }}
                />
            </LogoCard>
        </div>
    );
}

export default FormPage;