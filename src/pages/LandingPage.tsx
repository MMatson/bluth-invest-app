import React from 'react';
import { Button } from "@material-ui/core";
import LogoCard from "../components/LogoCard";

type Props = {
    onClick: () => void;
}

function LandingPage({ onClick }: Props) {
    return (
        <div className="LandingPage">
            <LogoCard title={"Welcome!"}
                      message={"Let's see if you qualify today."}>
                <div>
                    <Button className="button"
                            onClick={onClick}
                            variant="outlined"
                            disableElevation
                    >
                    Get Started
                    </Button>
                </div>
            </LogoCard>
        </div>
    );
}

export default LandingPage;
