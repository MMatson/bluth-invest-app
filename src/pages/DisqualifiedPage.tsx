import React from 'react';
import LogoCard from "../components/LogoCard";


function DisqualifiedPage() {
    return (
        <div className="DisqualifiedPage">
            <LogoCard title={"We're Sorry!"}
                      message={"You do not qualify at this time. Lorem ipsum..."}
            />
        </div>
    );
}

export default DisqualifiedPage;