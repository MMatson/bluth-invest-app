import React from 'react';
import LogoCard from "../components/LogoCard";


function ErrorPage() {
    return (
        <div className="ErrorPage">
            <LogoCard title={"Error: Bad Request"}
                      message={"Please try again later."}
            />
        </div>
    );
}

export default ErrorPage;