import React, { useState } from 'react';
import '../styles/App.css';
import LandingPage from "./LandingPage";
import ApprovedPage from "./ApprovedPage";
import DisqualifiedPage from "./DisqualifiedPage";
import FormPage from "./FormPage";
import { getInvestorApprovalFromAPI } from "../services/MockBackendService";
import { Values } from "../model/Values";
import ErrorPage from "./ErrorPage";

enum ApprovalStatus {
    WELCOME,
    UNSUBMITTED,
    APPROVED,
    DISQUALIFIED,
    ERROR
}

function App() {
    const [approvalStatus, setApprovalStatus] = useState(ApprovalStatus.WELCOME);

    function navigateToForm() {
        setApprovalStatus(ApprovalStatus.UNSUBMITTED)
    }

    function processForm(values: Values) {
        console.log(values);

        let promise = getInvestorApprovalFromAPI(values)
        promise.then(res => {
            if (res === "Approved") {
                setApprovalStatus(ApprovalStatus.APPROVED)
            } else {
                setApprovalStatus(ApprovalStatus.DISQUALIFIED)
            }
        })
        promise.catch(err => {
            setApprovalStatus(ApprovalStatus.ERROR)
        })
    }

    function renderPage(status: ApprovalStatus) {
        switch (status) {
            case ApprovalStatus.WELCOME:
                return <LandingPage onClick={navigateToForm}/>;
            case ApprovalStatus.UNSUBMITTED:
                return <FormPage onSubmit={ values => {
                    processForm(values);
                }}/>
            case ApprovalStatus.APPROVED:
                return <ApprovedPage/>
            case ApprovalStatus.DISQUALIFIED:
                return <DisqualifiedPage/>
            case ApprovalStatus.ERROR:
                return <ErrorPage/>
        }
    }

    return (
        <div className="App">
            {renderPage(approvalStatus)}
        </div>
    );
}

export default App;
