import React from 'react';
import LogoCard from "../components/LogoCard";


function ApprovedPage() {
    return (
        <div className="ApprovalPage">
            <LogoCard title={"Congratulations!"}
                      message={"You have been approved. For next steps, lorem ipsum..."}
            />
        </div>
    );
}

export default ApprovedPage;