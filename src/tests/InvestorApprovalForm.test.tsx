import React from 'react';
import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import InvestorApprovalForm from "../components/InvestorApprovalForm";

test('submit should not be allowed with initial form values', async () => {
    const onSubmit = jest.fn();
    render(<InvestorApprovalForm onSubmit={onSubmit} />);

    const submitButton = screen.getByText(/Submit/);
    await waitFor(() => {
        (fireEvent.click(submitButton));
    });

    //After attempting to submit, button should still be there
    expect(submitButton).toBeInTheDocument();
});

