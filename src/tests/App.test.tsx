import React from 'react';
import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import App from '../pages/App';

test('renders Get Started button to access form', () => {
  render(<App />);
  const startButton = screen.getByText(/Get Started/);
  expect(startButton).toBeInTheDocument();
});

test('button should navigate to form', () => {
  render(<App />);
  const startButton = screen.getByText(/Get Started/);
  fireEvent.click(startButton);
  const title = screen.getByText(/Investor Profile/);
  expect(title).toBeInTheDocument();
});
